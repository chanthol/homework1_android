package com.android.btb_003.homework01;

import java.io.Serializable;

public class Student implements Serializable{
    private String name,className;
    private Integer phone;

    public Student(String name, Integer phone, String className) {
        this.name = name;
        this.phone = phone;
        this.className = className;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", className='" + className + '\'' +
                '}';
    }
}
